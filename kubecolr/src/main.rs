use ratatui::{
    backend::CrosstermBackend,
    widgets::{Block, Borders, List, ListItem},
    layout::{Layout, Constraint, Direction},
    style::{Style, Color},
    Terminal,
};
use std::io;

fn main() -> Result<(), io::Error> {
    let stdout = io::stdout();
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    // Dummy Data
    let categories = vec![
        ("Deployment", vec!["app1", "app2"]),
        ("Service", vec!["svc1"]),
    ];

    // Main loop
    terminal.draw(|f| {
        let chunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints(
                categories
                    .iter()
                    .map(|_| Constraint::Length(3)) // Dynamically allocate space
                    .collect::<Vec<_>>(),
            )
            .split(f.size());

        for (i, (category, items)) in categories.iter().enumerate() {
            let mut list_items: Vec<ListItem> = vec![];

            // Add header
            list_items.push(ListItem::new(format!("▶ {}", category)));

            // Add resources if category is expanded
            for item in items {
                list_items.push(ListItem::new(format!("  - {}", item)));
            }

            let list = List::new(list_items)
                .block(Block::default().borders(Borders::ALL).title(category))
                .style(Style::default().fg(Color::White));

            f.render_widget(list, chunks[i]);
        }
    })?;

    Ok(())
}

