
kubectl create namespace monitoring
kubectl config set-context --current --namespace=monitoring


helm repo add prometheus-community https://prometheus-community.github.io/helm-charts\nhelm repo update
helm repo add grafana https://grafana.github.io/helm-charts\nhelm repo update
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx\nhelm repo update\nhelm install ingress-nginx ingress-nginx/ingress-nginx --namespace ingress-nginx --create-namespace\n

helm repo update

helm install prometheus-stack prometheus-community/kube-prometheus-stack
helm install grafana-infra grafana/grafana
helm install ingress-nginx ingress-nginx/ingress-nginx --namespace ingress-nginx --create-namespace
