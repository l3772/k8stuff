
# Kubernetes Labor

Dieser Artikel beschreibt den Aufbau einer Laborumgebung für einen Kubernetes Cluster. Wir beginnen mit einem Single Node Cluster, dieser kann später problemlos erweitert werden. Eine ausführlichere Anleitung bietet die Artikelserie [Container-Kompetenzoffensive in der c't](https://www.heise.de/select/ct/2022/22/2220016192019307305) von Jan Mahn. 


## Systemvoraussetzungen

Wir starten mit einem Ubuntu 22.04 Linux. Mein Computer ist eine HPE Z-Workstation mit Intel Xeon CPU (16Core, 3.9 GHz), 128GB RAM und zwei 4TB SSDs, man sollte aber auch mit deutlich weniger auskommen :-).

Eine der Festplatten ist mit ZFS formatiert, so dass später Integrationen mit NFS möglich sind.


### NFS-Server

Die grundlegende Einrichtung der ZSF-Pools beschreibt die [Ubuntu-Knowledgebase](https://ubuntu.com/tutorials/setup-zfs-storage-pool#1-overview). Die Freigabe eines Shares erfolgt ganz einfach mit 

```bash
$ sudo zfs set sharenfs=on storage/srv
```

Wenn der Server gestartet ist, sollte das Share zur Verfügung stehen; ein Checkup erfolgt mit:

```bash
$ systemctl status nfs-server.service
$ showmount -e
```


## K3S-Installation

Eine der einfachsten Möglichkeiten, einen Cluster aufzusetzen, ist, [K3s](https://k3s.io) zu verwenden. Erzeugen wir also zunächst auf dem ersten der Server die Konfigurationsdateien

```bash 
$ mkdir -p /etc/rancher/k3s/
$ cat > /etc/rancher/k3s/config.yaml
disable: traefik
^D
```

Das verhindert den Start des HTTP-Proxy Traefik, damit wir später lernen, Traefik manuell zu installieren. Auf dem ersten Server ist damit alles bereit für die Installation von K3s:

```bash
$ curl -sfL https://get.k3s.io | sh -s - server --cluster-init
```


## Werkzeugkiste

Ein sehr übersichtliches Tool für das Management von Kubernetes-Clustern ist [Lens](https://k8slens.dev/). 

![image](https://hackmd.io/_uploads/SJI-Bi2NT.png)

Kopiert man sich sich die Datei `/etc/rancher/k3s/k3s.yaml` nach `~/.kube/config` (und passt die Serveradresse an, damit sie nicht auf 127.0.0.1 zeigt), erkennt Lens die Control Plane automatisch und hilft, den Cluster zu verwalten.

```yaml

```

Es gibt ausserdem passende Container-Images, mit denen wir Probleme innerhalb des Clusters aufspüren können; eines davon ist [Netshoot](https://github.com/nicolaka/netshoot):

```yaml!
apiVersion: v1
kind: Pod
metadata:
  name: toolpod
spec:
  containers:
  - name: netshoot
    image: nicolaka/netshoot
    command: ["sleep"]
    args: ["3600"]
```

Das installiert Netshoot und schaltet ihn bei Nichtgebrauch ab. Wir können in den Container mit Lens einloggen oder mit `kubectl`:

```bash!
$ kubectl apply -f toolpod.yaml
$ kubectl exec -it toolpod -- zsh
```


## Cluster erweitern

Als nächstes brauchen wir ein Token, das das K3s auf der ersten Maschine in der Datei  `/var/lib/rancher/k3s/server/token` abgelegt hat. Kopiere das Token und setze auf dem neuen Host den folgenden Befehl ab:


```bash
curl -sfL https://get.k3s.io | K3S_TOKEN=<Token> sh -s - server --server https://<IP Server 1>:6443
```

Dabei ist `<Token>` die kopierte Zeichenkette und `Ip Server 1` die Adresse des zuerst eingerichteten Servers. Der neue Server wird mit diesem Befehl ebenfalls als Control Plane eingerichtet.

In Kubernetes wird der Begriff *Control Plane* verwendet, um die Komponenten zu beschreiben, die für das Management des Clusters verantwortlich sind. Diese Komponenten umfassen den API-Server, den Controller-Manager, den Scheduler und etcd (die Datenbank, die den Clusterzustand speichert).


## NFS-Storage einbinden

Um den vorhin angelegten NFS-Server einzubinden, ...


### Am Proxy vorbei

Wenn der Cluster hinter einem Corprate Web Proxy hängt, kann die Installation haarig werden, da evtl. den Zertifikaten nicht vertraut wird.

So geht es trotzdem:
```bash
$ helm repo add https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner
$ helm repo search nfs
$ helm pull nfs-ganesha-server-and-external-provisioner/nfs-server-provisioner
$ helm install nfs-provisioner nfs-server-provisioner-1.8.0.tgz
```


