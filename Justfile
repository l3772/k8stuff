ca_crt := "/var/lib/rancher/k3s/server/tls/client-ca.crt"
ca_key := "/var/lib/rancher/k3s/server/tls/client-ca.key"

default: build-key

build-key username:
    @openssl genrsa -out {{username}}.key 2048
    @openssl req -new -key {{username}}.key -out {{username}}.csr -subj "/CN={{username}}/O=ktag.ch"
    @sudo openssl x509 -req -in {{username}}.csr -CA {{ca_crt}} -CAkey {{ca_key}} -CAcreateserial -out {{username}}.crt -days 364