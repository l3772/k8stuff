#/usr/bin/zsh

# set -x
#

setup_k3s() {
  curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="server --disable=traefik" sh -
}


copy_local_config() {
  sudo cp /etc/rancher/k3s/k3s.yaml ~/.kube/config
  sudo chown $USER:$GROUP ~/.kube/config
  chmod 600 ~/.kube/config
}


setup_nfs() {
  NFS_SERVER_NAME=$(hostname -f)
  NFS_SERVER_PATH='/storage/srv'
  envsubst < nfs-provisioner.yaml | sudo tee /var/lib/rancher/k3s/server/manifests/nfs.yaml
}


add_cluster_node() {
  echo "Perform the following command on the new node:"
  TOKEN=$(sudo cat /var/lib/rancher/k3s/server/token)
  echo "curl -sfL https://get.k3s.io | K3S_TOKEN=${TOKEN} sh -s - server --server https://${HOST}:6443"
}

# Install K3s if not yet installed
[ -f /etc/rancher/k3s/k3s.yaml ] || setup_k3s
copy_local_config
add_cluster_node

